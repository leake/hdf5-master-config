import xml.parsers.expat,sys
from glob import glob
import lxml.etree as etree

if len(sys.argv) != 2:
    print '''Usage:

python %s filename

''' % sys.argv[0]
    sys.exit(0)


def getSubentries(filename):
     techniques = []
     subentries = etree.parse(filename).xpath('//group[@NX_class="NXsubentry"]')
     for subentry in subentries:                               
                if subentry.attrib.has_key('groupName'):
                    techniques.append(subentry.get('groupName'))
     return techniques

for arg in sys.argv[1:]:
    for filename in glob(arg):
        try:
            techniques = getSubentries(filename)
            print techniques           
                     
        except Exception, e:
            print "[ERROR] %s is %s" % (filename, e)
