#! /usr/bin/python
#
# Search the ICAT for all entity types and report the number of
# objects found for each type.
#
from __future__ import print_function
import icat
import icat.config
import logging
import sys
import csv
from distutils.util import strtobool


logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.CRITICAL)

'''
	It returns an array with the text nodes of a XML file
'''
def getParameterListFromFilePath(filePath):
    tree = ET.parse(filePath) 
    text = (ET.tostring(tree.getroot(), encoding='utf8', method='text'))
    noBlankLines = filter(lambda x: not re.match(r'^\s*$', x), text)
    return noBlankLines.replace("$", "\n").replace("{", "").replace("}", "").split("\n")


def getParameterListFromICAT(conf):    
    client = icat.Client(conf.url, **conf.client_kwargs)
    client.login(conf.auth, conf.credentials)

    try:
        parameterTypeList = client.search("SELECT (p) FROM ParameterType p"  )
        return sorted(parameterTypeList, key=lambda k: k.name)          
    except icat.exception.ICATPrivilegesError:   
        print ("[ERROR] %s" % ( e)   )

print(icat.config.Config().getconfig())
parameterTypeList = getParameterListFromICAT(icat.config.Config().getconfig())

for parameter in parameterTypeList:    
    print("%s"% (parameter.name))        
    
