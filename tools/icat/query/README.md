## Query

Example of query to ICAT

### Run
```
python list.py -s test --no-check-certificate -c icat.cfg --https-proxy ""
```


### Credentials

icat.cfg needs to be present in the folder
```
[test]
url = https://ovm-icat-test.esrf.fr:8181/ICATService/ICAT?wsdl
auth = db
username = *****
password = *****
idsurl = https://ovm-icat2:8181/ids
# uncomment, if your server does not have a trusted certificate
checkCert = No
```

