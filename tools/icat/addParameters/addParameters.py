#! /usr/bin/python
#
# Create some parameter types, (actually just one for testing atm).
#
from __future__ import print_function
import icat
import icat.config
import sys
import logging
from sets import Set
import xml.etree.ElementTree as ET
import re
from distutils.util import strtobool

def user_yes_no_query(question):
    sys.stdout.write('%s [y/n]\n' % question)
    while True:
        try:
            return strtobool(raw_input().lower())
        except ValueError:
            sys.stdout.write('Please respond with \'y\' or \'n\'.\n')	



logging.basicConfig(level=logging.INFO)
conf = icat.config.Config().getconfig()
client = icat.Client(conf.url, **conf.client_kwargs)
client.login(conf.auth, conf.credentials)

# ------------------------------------------------------------
# Get parameters type from ICAT we need later on
# ------------------------------------------------------------
parametersType = client.search("ParameterType")
parametersList = []
for parameterType in parametersType:
	parametersList.append(parameterType.name)
dictionary = (set(parametersList))


# ------------------------------------------------------------
# Get parameters from xml file
# ------------------------------------------------------------
tree = ET.parse('../../../hdf5_cfg.xml') 
text = (ET.tostring(tree.getroot(), encoding='utf8', method='text'))
noBlankLines = filter(lambda x: not re.match(r'^\s*$', x), text)
attributeList = noBlankLines.replace("$", "\n").replace("{", "").replace("}", "").split("\n")[1:]


# -------------------
# Removing duplicates
# -------------------
attributeList = list(set(attributeList))
# ------------------------------------------------------------
# This compares the list of parameters of the database and the list of parameters of the xml
# ------------------------------------------------------------
parametertype_data = []
for attribute in attributeList:
	if attribute in dictionary:
		print(attribute + " already exists")
	else:
		parametertype_data.append( {
			'name': attribute,
			'units' : 'NA',
			'unitsFullName' : '',
			'valueType': "STRING",
		})

def createParameter(pdata, client):
    parametertypes = []
    print("\n[CREATING] ParameterType: '%s' " % pdata['name'])
    parametertype = client.new("parameterType")
    parametertype.name = pdata['name']
    parametertype.units = pdata['units']
    parametertype.unitsFullName = pdata['unitsFullName']
    parametertype.valueType = pdata['valueType']
    parametertype.applicableToDatafile = False
    parametertype.applicableToDataset = True
    parametertype.applicableToSample = True
    parametertype.applicableToInvestigation = False
    parametertype.facility = hzb
    parametertypes.append(parametertype)
    client.createMany(parametertypes)	
    print("[CREATED] ParameterType: '%s' " % pdata['name'])
    print("[CHECKING] ParameterType: '%s' " % pdata['name'])
    parameterTypeList = client.search("SELECT (p) FROM ParameterType p where p.name = '%s'" % parametertype.name  )
    print(parameterTypeList)	
    if (len(parameterTypeList) > 0):
        print("[SUCCESS] ParameterType: '%s' has successfully been created" % pdata['name'])
    else:
        print("[ERROR] ParameterType: '%s' has not been created" % pdata['name'])

# ------------------------------------------------------------
# Storing parameters type on ICAT
# ------------------------------------------------------------
hzb = client.assertedSearch("Facility[name='ESRF']")[0]

for pdata in parametertype_data:
    if (user_yes_no_query("\n[QUESTION] Do you want to create %s?" % str(pdata['name'])) == False):
        print ("\tSkipping parameterType %s" %pdata['name'])
    else:                
        createParameter(pdata, client )

        


